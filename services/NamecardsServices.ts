import { Knex } from 'knex';
import { NameInfo } from './models';

export class NamecardsService {

    constructor(private knex: Knex) {

    }

    async loadBizCards(id: number, cat_id: number, txtInput: string | undefined) {

        let query = this.knex.select(['namecards.*', 'category.category_name']).from('namecards').innerJoin('category', 'category_id', 'category.id'); 

        if (Number.isNaN(cat_id) && txtInput == undefined) {
            query = query.where('namecards.users_id', id).orderBy('name');
        } else {
            if (!Number.isNaN(cat_id)) {
                query = query.andWhere('category_id', cat_id);
            }
            if (txtInput) {
                query = query.andWhere(this.knex.raw(`LOWER(name) like LOWER('%${txtInput}%')`)).orWhere(this.knex.raw(`LOWER(org) like LOWER('%${txtInput}%')`));
            }
            query = query.orderBy('name'); 
        }

        return await query;

    }
    
    async checkcate(userid: string, cateName: string, nameInfo: NameInfo) {
        const trx = await this.knex.transaction();
        try {
            let cate = await trx.select('id').from('category').where('category_name', cateName)

            let cateId = 0;
            if (cate.length > 0) {
                cateId = (await trx.select('id').from('category').where('category_name', cateName).first()).id;

            } else {
                let cateArr = await trx('category').insert({ 'category_name': cateName, 'users_id': userid }).returning('id');
                cateId = cateArr[0]
            }

            await trx('namecards').insert({
                'users_id': userid,
                'name': nameInfo.fullName,
                'org': nameInfo.company,
                'address': nameInfo.addresses,
                'phone1': nameInfo.phone1,
                'phone2': nameInfo.phone2,
                'email': nameInfo.email,
                'title': nameInfo.title,
                'website': nameInfo.websites,
                'linkedin': nameInfo.linkedin,
                'category_id': cateId
            })
            await trx.commit();
        } catch (error) {
            await trx.rollback();
            throw error;
        }
    }

    async deleteCard(cardId: number) {
        await this.knex('namecards').where('id', cardId).del(); 
        return await this.knex.select('*').from('namecards'); 
    }

}