async function loadBizCards() {
    const res = await fetch('/biz-cards');

    const bizCards = await res.json();
    console.log(bizCards);

    for (let i = 0; i < bizCards.length; i++) {
        document.querySelector('.card-container')
            .innerHTML += `
                <div class="card-group">
                    <div class="card-field">
                        <div class="info-group">
                            <div class="card-name">${bizCards[i].name}</div>
                            <div class="company-name">${bizCards[i].org}</div>
                            <div class="job-title">${bizCards[i].title}</div>
                        </div>
                        <div class="contact-group">
                            ${bizCards[i].address ? `
                                <div class="flex-group address-group">
                                    <div class="address">${bizCards[i].address}</div>
                                </div>` : ""}
                            ${bizCards[i].phone1 ? `
                                <div class="flex-group phone-group">
                                    <i class="fas fa-phone-alt"></i>
                                    <div class="contact">${bizCards[i].phone1}</div>
                                </div>` : ""}
                            ${bizCards[i].email ? `
                                <div class="flex-group email-group">
                                    <i class="far fa-envelope"></i>
                                    <div class="email">${bizCards[i].email}</div>
                                </div>` : ""}
                        </div>
                    </div>
                    <div class="delete-icon" data-index="${bizCards[i].id}">
                        <i class="fas fa-trash-alt"></i>
                    </div>
                </div>
            `

    }
    const deleteBtns = document.querySelectorAll('.delete-icon');

    for (let deleteBtn of deleteBtns) {
        deleteBtn.addEventListener('click', async (event) => {
            event.preventDefault();

            const cfn = confirm('Confirm delete?');

            if (cfn) {
                const delBtnIdx = event.currentTarget.getAttribute('data-index');

                const res = await fetch(`/biz-cards?id=${delBtnIdx}`, {
                    method: "DELETE"
                });

                if (res.status === 200) {
                    window.location = '/biz-cards.html';
                } else {
                    await res.json();
                }
            } else {
                event.stopPropagation();
            }
        })
    }

    let cat_name_arr = [];
    for (let bizCard of bizCards) {
        cat_name_arr.push({
            category_name: bizCard.category_name,
            category_id: bizCard.category_id
        });
    }
    console.log(cat_name_arr.findIndex(t =>
        // findIndex() method returns the index of the first element in the array that satisfies the provided testing function.
        // t: The current element being processed in the array.
        (t.category_name == "Fashion")
    ))

    filtered_arr = cat_name_arr.filter((element, index, array) =>
        // filter() method creates a new array with all elements that pass the test implemented by the provided function.
        // element: The current element being processed in the array.
        // index: The index of the current element being processed in the array.
        // array: The array filter was called upon.
        array.findIndex(t =>
            // findIndex() method returns the index of the first element in the array that satisfies the provided testing function.
            // t: The current element being processed in the array.
            (t.category_name === element.category_name)
        ) === index
    )
    console.log(filtered_arr);
    for (let i = 0; i < filtered_arr.length; i++) {
        console.log(bizCards[i].category_id)
        document.querySelector('#cat')
            .innerHTML += `
            <option value="${filtered_arr[i].category_id}">${filtered_arr[i].category_name}</option>
            `
    }


}

loadBizCards();

