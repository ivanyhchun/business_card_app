import * as a from './modules/interact.js'
import * as b from './modules/html2canvas.js'


const namecardzone = document.querySelector('.namecard-zone')
let testboxId = 1
let imageId = 1
let profileId = window.location.search.replace("?id=", "")

// load initial text field
async function initialize(profileId) {
    const res = await fetch(`/edit-my-card/?id=${profileId}`);
    const result = await res.json()
    for (let field in result) {
        if (result[field] != "" && result[field] != null && field != 'id' && field != 'users_id' && field != 'category_id' && field != 'profile_name' && field != 'namecard_img' && field != 'created_at' && field != 'updated_at') {
            namecardzone.innerHTML += `<div class="droppable droppable-text" id="testbox-${testboxId}">${result[field]}</div>`
            testboxId++;
        }
    }
}

initialize(profileId);


document.querySelector('.add-element').addEventListener('click', () => {
    namecardzone.innerHTML += `<div class="droppable droppable-text" id="testbox-${testboxId}"> New Text Box </div>`
    testboxId++
    gesturableListener();
})


document.querySelector('.upload-Bg').addEventListener('change', async (event) => {
    event.preventDefault();
    let formData = new FormData();
    formData.append('image', event.target.files[0]);
    console.log(profileId)
    const res = await fetch(`/upload-card-bg/?id=${profileId}`, {
        method: "POST",
        body: formData
    });
    if (res.status === 200) {
        const result = await res.json();
        document.querySelector('.background-img').src = `./nameCards/${result}`
    }
})


document.querySelector('.form-control').addEventListener('change', async (event) => {
    event.preventDefault();
    const formData = new FormData();
    formData.append('image', event.target.files[0]);
    const res = await fetch('/upload-pic', {
        method: "POST",
        body: formData,
    })
    console.log(res);
    if (res.status === 200) {
        const result = await res.json();
        namecardzone.innerHTML += `<div class = 'droppable droppable-image' id="image-${imageId}"><img src="/uploads/${result.path}" alt="sample image" class="scale-element"></div>`
        gesturableListener();
    } else {
        alert("Upload failed. Try again")
    }
})


document.querySelector('.upload').addEventListener('click', async () => {
    const req = {
        content: namecardzone.innerHTML
    }
    if (req) {
        const res = await fetch(`/draw-card/${profileId}`, {
            method: "POST",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify(req)
        });
        if (res.status == 200) {
            alert("Upload success!")
        } else {
            alert("Upload failed. Please try again.")
        }
    }
})

document.querySelector('.download').addEventListener('click', async () => {
    const res = await fetch(`/draw-card/${profileId}`);
    const result = await res.json()
    namecardzone.innerHTML = result.html;
    gesturableListener();
})

document.querySelector('.export').addEventListener('click', () => {
    html2canvas(document.querySelector('.namecard-zone')).then(async function (canvas) {
        let base64URL = canvas.toDataURL('image/png');

        // Trigger download the namecard image
        var a = document.createElement("a");
        a.href = base64URL;
        a.download = "Namecard.png";
        a.click();

        // Upload base64 to server
        const res = await fetch(`/DwgToPNG/${profileId}`, {
            method: "POST",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify({
                base64URL: base64URL
            })
        });
        if (res.status != 200) {
            alert("Upload failed. Please try again.")
        }
    })
})

// // essential function for interact.js to be worked
function dragMoveListener(event) {
    var target = event.target
    var x = (parseFloat(target.getAttribute('data-x')) || 0) + event.dx
    var y = (parseFloat(target.getAttribute('data-y')) || 0) + event.dy

    target.style.transform = 'translate(' + x + 'px, ' + y + 'px)'

    target.setAttribute('data-x', x)
    target.setAttribute('data-y', y)
}
// this function is used later in the resizing and gesture demos
window.dragMoveListener = dragMoveListener

function gesturableListener() {
    let angleScaleList = []
    let gestureAreaList = document.querySelectorAll('.droppable-image')
    let scaleElementList = document.querySelectorAll('.scale-element')

    for (let i = 0; i < gestureAreaList.length; i++) {
        angleScaleList.push({
            angle: 0,
            scale: 1
        })
        interact(gestureAreaList[i])
            .gesturable({
                listeners: {
                    start(event) {
                        angleScaleList[i].angle -= event.angle
                    },
                    move(event) {
                        var currentAngle = event.angle + angleScaleList[i].angle
                        var currentScale = event.scale * angleScaleList[i].scale
                        scaleElementList[i].style.transform =
                            'rotate(' + currentAngle + 'deg)' + 'scale(' + currentScale + ')'
                        dragMoveListener(event)
                    },
                    end(event) {
                        angleScaleList[i].angle = angleScaleList[i].angle + event.angle
                        angleScaleList[i].scale = angleScaleList[i].scale * event.scale
                    }
                }
            })
            .draggable({
                listeners: { move: dragMoveListener }
            })
            .resizable({
                // resize from all edges and corners
                edges: { left: false, right: true, bottom: true, top: false },

                listeners: {
                    move(event) {
                        var target = event.target
                        var x = (parseFloat(target.getAttribute('data-x')) || 0)
                        var y = (parseFloat(target.getAttribute('data-y')) || 0)

                        // update the element's style
                        target.style.width = event.rect.width + 'px'
                        target.style.height = event.rect.height + 'px'

                        // translate when resizing from top or left edges
                        x += event.deltaRect.left
                        y += event.deltaRect.top

                        target.style.transform = 'translate(' + x + 'px,' + y + 'px)'

                        target.setAttribute('data-x', x)
                        target.setAttribute('data-y', y)
                        // target.textContent = Math.round(event.rect.width) + '\u00D7' + Math.round(event.rect.height)
                    }
                },
                modifiers: [
                    // minimum size
                    interact.modifiers.restrictSize({
                        min: { width: 50, height: 20 }
                    })
                ],
                inertia: true
            })
    }
}
// Namecard Dropzone Definition


interact('.namecard-zone')
    .dropzone({
        accept: '.droppable',
        overlap: 0.75,

        ondropactivate: function (event) {
            event.target.classList.add('drop-active')
        },
        ondragenter: function (event) {
            var draggableElement = event.relatedTarget
            var dropzoneElement = event.target

            dropzoneElement.classList.add('drop-target')
            draggableElement.classList.add('can-drop')
            // draggableElement.textContent = 'Dragged in'
        },
        ondragleave: function (event) {
            event.target.classList.remove('drop-target')
            event.relatedTarget.classList.remove('can-drop')
            // event.relatedTarget.textContent = 'Dragged out'
        },
        ondrop: function (event) {
            event.relatedTarget.classList.add('dropped')
            // event.relatedTarget.textContent = 'Dropped'
        },
        ondropdeactivate: function (event) {
            event.target.classList.remove('drop-active')
            event.target.classList.remove('drop-target')
        }
    })
// elements with drag-and-drop, resizable and content editable attributes
interact('.droppable-text')
    .draggable({
        listeners: { move: window.dragMoveListener },
        inertia: true,
        modifiers: [
            interact.modifiers.restrictRect({
                endOnly: true
            })
        ]
    })
    .resizable({
        // resize from all edges and corners
        edges: { left: false, right: true, bottom: true, top: false },

        listeners: {
            move(event) {
                var target = event.target
                var x = (parseFloat(target.getAttribute('data-x')) || 0)
                var y = (parseFloat(target.getAttribute('data-y')) || 0)

                // update the element's style
                target.style.width = event.rect.width + 'px'
                target.style.height = event.rect.height + 'px'
                

                // translate when resizing from top or left edges
                x += event.deltaRect.left
                y += event.deltaRect.top

                target.style.transform = 'translate(' + x + 'px,' + y + 'px)'
                let newSize = Math.floor(parseInt(target.style.height)*1)
                target.style.fontSize = newSize + "px"


                target.setAttribute('data-x', x)
                target.setAttribute('data-y', y)
                // target.textContent = Math.round(event.rect.width) + '\u00D7' + Math.round(event.rect.height)
            }
        },
        modifiers: [
            // minimum size
            interact.modifiers.restrictSize({
                min: { width: 50, height: 20 }
            })
        ],
        inertia: true
    })
    .on('doubletap', function (event) {
        event.currentTarget.setAttribute("contenteditable", "true");
    })


//  Trash bin Dropzone Definition
interact('.trash-zone')
    .dropzone({
        accept: '.droppable',
        // accept: '.droppable-image',
        overlap: 0.01,

        ondropactivate: function (event) {
            event.target.classList.add('drop-active')
        },
        ondragenter: function (event) {
            var draggableElement = event.relatedTarget
            var dropzoneElement = event.target

            dropzoneElement.classList.add('drop-target')
            draggableElement.classList.add('will-remove')
        },
        ondragleave: function (event) {
            event.target.classList.remove('drop-target')
            event.relatedTarget.classList.remove('will-remove')
        },
        ondrop: function (event) {
            event.relatedTarget.remove()
        },
        ondropdeactivate: function (event) {
            event.target.classList.remove('drop-active')
            event.target.classList.remove('drop-target')
        }
    })
