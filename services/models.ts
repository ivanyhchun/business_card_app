export interface User {
    email:string;
    display_name:string ;
    password: string;
    linkedin_ac?:string;
    google_ac?:string;
    facebook_ac?:string
}

export interface DrawingElement {
    profileId:string;
    html:string
}

export interface Namecard {
    name?:string;
    company?:string;
    addresses?:string;
    phone1?:string;
    phone2?:string;
    title?:string;
    email?:string;
    websites?:string;
    image?:string;
    
}
export interface NameInfo{
  fullName?:string;
  company?:string;
  title?:string;
  addresses?:string;
  phone1?:string;
  phone2?:string;
  email?:string;
  websites?:string;
  linkedin?:string;
  category_id?:number;
}