* yarn init -y
* yarn add jest
* yarn add typescript ts-jest @types/jest @types/node ts-node ts-node-dev 
* yarn add dotenv @types/dotenv knex @types/knex pg @types/pg xlsx @types/xlsx
* yarn add express-session @types/express-session express @types/express bcryptjs @types/bcryptjs
* yarn add --dev playwright 
* yarn ts-jest config:init
* yarn knex init -x ts
* psql -u postgres
* \l -> list all database ,\d database -> jump into that database 
* yarn test --coverage
* yarn ts-node xxxx.ts
* yarn knex migrate:make create-teachers -x ts
* yarn knex migrate:latest --env test
* yarn knex seed:make -x ts create-teachers-and-students
* yarn knex seed:run --env test
* yarn knex migrate:up

-------------------------------------

# Python
* https://stackoverflow.com/questions/44515769/conda-is-not-recognized-as-internal-or-external-command
* conda install pandas
* conda list -e > requirements.txt
* conda install --file requirements.txt
* conda install scikit-learn
* conda install -c anaconda python=3.8
* conda create --name tf_python python=3.8 , conda activate tf_python, conda list
* conda install -c anaconda tensorflow
* conda install -c anaconda tensorflow-datasets
* conda install matplotlib
* conda install -c conda-forge sanic

# Namecard recognization
* yarn add @azure/ai-form-recognizer@3.1.0-beta.3

# essential scripts to be added in package.json
  "scripts": {
    "dev": "ts-node-dev main.ts",
    "import": "ts-node import.ts",
    "format": "prettier --write .",
    "start": "ts-node main.ts",
    "test": "jest"
},

# essential scripts to be added in knexfile.ts
import dotenv from 'dotenv';
dotenv.config();
------------------------------------------------------
development: {
    debug: true ,
    client: "postgresql",
    connection: {
      database: process.env.DB_NAME,
      user: process.env.DB_USERNAME,
      password: process.env.DB_PASSWORD
    },
    pool: {
      min: 2,
      max: 10
    },
    migrations: {
      tableName: "knex_migrations"
    }
  },
  test:{
      client: 'postgresql',
      connection: {
          database: process.env.TESTDB_NAME,
          user:     process.env.DB_USERNAME,
          password: process.env.DB_PASSWORD
      },
      pool: {
          min: 2,
          max: 10
      },
      migrations: {
          tableName: 'knex_migrations'
      } 
  },
  staging: {
      //...
  },



# Steps of converting Typescript to MVC architecture
1. Create "Services" and "Controllers" folder
2. create "xxServices.ts" and "xxController.ts" 
3. Create sequence: first "services", then 'controllers', considering the dependency of imports
4. in 'xxServices.ts', export class MemoServices{
        constructor(private client:Client)      // dependency injection
    }
    // define class methods
    async listMemo():Promise<Memo[]>{
        //copy the original SQL query code here
        // return the result
    }
5. Sometime may need to create the interface in models.ts yourself (in normal case, VSCode will do for you)
6. in 'xxController.ts', import the class of MemoServices just being created
7. Construct class 'Memo Controller' {
    constructor (private memoService:MemoService){
        // define class method with arrow function to perform the request / response (i.e. the Controller function)
    async getMemos = async (req:express.Request,res:express.Response)=>{
    }}}
8. In main.ts, export ** according to the sequence:
    a) export memoService = new MemoService(client)  <--- to insert the dependency into service. In this case, the client information of DB login details
    b) export memoController = new MemoController(memoService)  <-- to insert the 2nd level dependency into controller. IN this case, the Service with the client information.
9. in memoRoutes.ts, convert the original function to the following style:

    memoRoutes.get("/memos",memoController.getMemos);
    memoRoutes.delete("/memos/:id",isLoggedInAPI (Keep unchanged), memoController.deleteMemo) 