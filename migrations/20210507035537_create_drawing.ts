import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {
    const hasTable = await knex.schema.hasTable('drawings');
    if(!hasTable){
        await knex.schema.createTable('drawings',(table)=>{
            table.increments();
            table.integer('profile').unsigned().unique();
            table.foreign('profile').references('profile.id');
            table.text('html');
            table.timestamps(false,true); 
        })
    }
}


export async function down(knex: Knex): Promise<void> {
    await knex.schema.dropTableIfExists('drawings')
}

