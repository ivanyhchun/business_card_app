import express from 'express';
import path from 'path';
import { Namecard } from '../services/models';
import { NamecardsService } from '../services/NamecardsServices';
import { recognizeCards } from './recognizeNamecard';

export class NamecardsController {

    constructor(private namecardsService: NamecardsService) {

    }

    loadBizCards = async (req: express.Request, res: express.Response) => {
        try {
            const cat_id = parseInt(String(req.query.cat_id));
            let txtInput = req.query.txtInput;
            if (txtInput !== undefined)
                txtInput = txtInput as string

            const id = req.session['user'].id;
            
            const result = await this.namecardsService.loadBizCards(id, cat_id, txtInput);
            res.status(200).json(result);

        } catch (error) {
            console.log(error);
            res.status(401).json({ success: false });
        }
    }


    uploadNamecards = async (req: express.Request, res: express.Response) => {
        
        try {
            if (req.file) {
                const nameCardImg = req.file.filename;
                const imgPath = path.join(__dirname, '../protected/nameCards/') + nameCardImg
                const businessCard = await recognizeCards(imgPath)

                let namecardName = "";
                let namecardCompanyNames = "";
                let namecardEmail = "";
                let namecardAddresses = "";
                let namecardPhone = "";
                let namecardWorkPhone = "";
                let namecardJobTitles = "";
                let namecardWebsites = "";

                if (businessCard.fields["ContactNames"]?.value[0].value["FirstName"].value) {
                    namecardName = businessCard.fields["ContactNames"].value[0].value["FirstName"].value + " " + businessCard.fields["ContactNames"].value[0].value["LastName"].value
                    
                }
                if (businessCard.fields["CompanyNames"]?.value[0].value) {
                    namecardCompanyNames = businessCard.fields["CompanyNames"].value[0].value
                    
                }

                if (businessCard.fields["Addresses"]?.value[0].value) {
                    namecardAddresses = businessCard.fields["Addresses"].value[0].value
                    
                }
                if (businessCard.fields["MobilePhones"]?.value[0].value) {
                    namecardPhone = businessCard.fields["MobilePhones"].value[0].value
                    
                }
                if (businessCard.fields["WorkPhones"]?.value[0].value) {
                    namecardWorkPhone = businessCard.fields["WorkPhones"].value[0].value
                    
                }
                if (businessCard.fields["Emails"]?.value[0].value) {
                    namecardEmail = businessCard.fields["Emails"].value[0].value
                    
                }
                if (businessCard.fields["JobTitles"]?.value[0].value) {
                    namecardJobTitles = businessCard.fields["JobTitles"].value[0].value
                    
                }
                if (businessCard.fields["Websites"]?.value[0].value) {
                    namecardWebsites = businessCard.fields["Websites"].value[0].value
                    
                }
                const namecard: Namecard = {
                    name: namecardName,
                    company: namecardCompanyNames,
                    addresses: namecardAddresses,
                    phone1: namecardPhone,
                    phone2: namecardWorkPhone,
                    email: namecardEmail,
                    title: namecardJobTitles,
                    websites: namecardWebsites,
                    image: nameCardImg

                }
                

                res.status(200).json(namecard)
            }
        } catch (error) {
            console.log(error);
            res.status(401).json({ success: false })
        }
    }

    createScanCard = async (req: express.Request, res: express.Response) => {
        try {
            const userId = req.session['user'].id;
            console.log(req.body)
            console.log(req.body.category)

            let cateName = req.body.category
            let nameInfo = req.body;
            await this.namecardsService.checkcate(userId, cateName, nameInfo)


            res.status(200).json({ success: true })

        } catch (error) {
            console.log(error);
            res.status(401).json({ success: false });
        }
    }

    deleteCard = async (req: express.Request, res: express.Response) => {
        try {
            const cardId = parseInt(String(req.query.id));

            const result = await this.namecardsService.deleteCard(cardId);
            res.status(200).json(result);

        } catch (error) {
            res.status(401).json({ success: false });
            console.log(error);
        }
    }

  
}



