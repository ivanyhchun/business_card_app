async function updateFunction() {
    document.querySelector('.form-control-group')
        .addEventListener('submit', async(event) => {
            event.preventDefault(); 

            const form = event.target; 
            const formData = new FormData(); 
            // console.log(form.image.files[0]); 
            formData.append('display_name', form.display_name.value); 
            if(form.image.files[0]) {
                formData.append('image', form.image.files[0]); 
            }
            // console.log(formData); 
            const res = await fetch('/edit-content', {
                method: 'POST',
                body: formData,
            }); 

            if(res.status === 200) {
                const result = await res.json();
                window.location = './profile.html'; 
            }
            
            form.reset(); 
        })
}
updateFunction(); 