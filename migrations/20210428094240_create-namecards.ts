import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {
    const hasTable = await knex.schema.hasTable('namecards');
    if(!hasTable){
        await knex.schema.createTable('namecards',(table)=>{
            table.increments();
            table.integer('users_id').unsigned();
            table.foreign('users_id').references('users.id')
            table.string("eng_name");
            table.string("eng_org");
            table.text("eng_address");
            table.string("phone1");
            table.string("phone2");
            table.string("email");
            table.string("eng_title");
            table.string("website");
            table.string("instagram");
            table.string("linkedin");
            table.integer('category_id').unsigned();
            table.foreign('category_id').references('category.id'); 
            table.text("additional_field"); 
            table.timestamps(false,true); 
        })
    }
}


export async function down(knex: Knex): Promise<void> {
    await knex.schema.dropTableIfExists('namecards')
}

