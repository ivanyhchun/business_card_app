document.querySelector('.search-box')
    .addEventListener('submit', async (event) => {
        event.preventDefault();

        const cat_id = document.querySelector('#cat').value;
        console.log(cat_id);

        const txtInput = document.querySelector('.search-bar').value;
        console.log(txtInput);

        let route = "/biz-cards";
        if (cat_id || txtInput) {
            route += "?";
        }
        if (cat_id) {
            route += `cat_id=${cat_id}`;
        }
        if (txtInput) {
            route += `&txtInput=${txtInput}`;
        }

        const res = await fetch(route);
        const bizCards = await res.json();
        console.log(bizCards);
        document.querySelector('.card-container')
            .innerHTML = "";

        for (let i = 0; i < bizCards.length; i++) {
            document.querySelector('.card-container')
                .innerHTML += `
                <div class="card-group">
                    <div class="card-field">
                        <div class="info-group">
                            <div class="card-name">${bizCards[i].name}</div>
                            <div class="company-name">${bizCards[i].org}</div>
                            <div class="job-title">${bizCards[i].title}</div>
                        </div>
                        <div class="contact-group">
                            ${bizCards[i].address ? `
                                <div class="flex-group address-group">
                                    <div class="address">${bizCards[i].address}</div>
                                </div>` : ""}
                            ${bizCards[i].phone1 ? `
                                <div class="flex-group phone-group">
                                    <i class="fas fa-phone-alt"></i>
                                    <div class="contact">${bizCards[i].phone1}</div>
                                </div>` : ""}
                            ${bizCards[i].email ? `
                                <div class="flex-group email-group">
                                    <i class="far fa-envelope"></i>
                                    <div class="email">${bizCards[i].email}</div>
                                </div>` : ""}
                        </div>
                    </div>
                    <div class="delete-icon" data-index="${bizCards[i].id}">
                        <i class="fas fa-trash-alt"></i>
                    </div>
                </div>
            `
        }

        const deleteBtns = document.querySelectorAll('.delete-icon');

        for (let deleteBtn of deleteBtns) {
            deleteBtn.addEventListener('click', async (event) => {
                event.preventDefault();
                event.stopPropagation();

                const cfn = confirm('Confirm delete?');

                if (cfn) {
                    const delBtnIdx = event.currentTarget.getAttribute('data-index');

                    const res = await fetch(`/biz-cards?id=${delBtnIdx}`, {
                        method: "DELETE"
                    });

                    if (res.status === 200) {
                        window.location = '/biz-cards.html';
                    } else {
                        await res.json();
                    }

                }
            })
        }
    })


