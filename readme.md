# Business Card Project
This app is aimed at helping users to manage business cards. Preliminary idea of functions include:
* Scan, digitalize and save business card
* Filter, Sort and Screen saved business cards,
* Save user's personal info and create their own business cards