let loginContainer = document.querySelector('.login-container');
let signupContainer = document.querySelector('.signup-container');
let btnClicked = false;

document.querySelector('.signup-btn')
    .addEventListener('click', () => {
        if (!btnClicked) {
            loginContainer.style = 'display: none; ';
            signupContainer.style = 'display: block; ';
            signupFunction();
        }
    })

document.querySelector('.signin-btn')
    .addEventListener('click', () => {
        loginContainer.style.display = 'block'; 
        signupContainer.style.display = 'none'; 
    })    


async function signupFunction(){
    document.querySelector('.signup-body')
        .addEventListener('submit',async (event) =>{
            event.preventDefault();
            const form = event.target;
            let signupInfo = {};
            if(form.display_name.value.length == 0 || form.email.value.length == 0 || form.password.value.length == 0 || form.confirm_password.value.length == 0){
                alert("All field must be filled")
            } else {
                if (form.password.value == form.confirm_password.value){
                    signupInfo.display_name = form.display_name.value;
                    signupInfo.email = form.email.value;
                    signupInfo.password = form.password.value
                    const res = await fetch('/signup', {
                        method: "POST",
                        headers: {
                            "Content-Type": "application/json"
                        },
                        body: JSON.stringify(signupInfo)
                    });
                    console.log(res)
                    if (res.status === 201) {
                        console.log(await res.json());
                        window.location = '/main.html'
                    } else if (res.status === 406) {
                        alert('Username already exist. Please try another username')
                        // document.querySelector(".signup-container .show-message").innerText = 'Username already exist. Please try another username'
                        form.reset();
                    } else {
                        alert(res.message)
                        // document.querySelector(".signup-container .show-message").innerText = res.message
                    }
                } else {
                    alert("Password confirm failed. Please check the spelling")
                    // document.querySelector(".signup-container .show-message").innerText = "Password confirm failed. Please check the spelling"
                    form.password.value = "";
                    form.confirm_password.value = "";
                }
            }
        })
}

async function getCurrentUser() {
    try {
        const res = await fetch('/current-user');
        const result = await res.json();

        if (res.status === 200) {
            currentUser = result;
        }
        return currentUser;
    } catch (err) {
        console.log(err);
    }
}
    
document.querySelector('.login-body')
    .addEventListener('submit', async (event) => {
        event.preventDefault();
        const form = event.target;
        let loginInfo = {};
        loginInfo.email = form.email.value;
        loginInfo.password = form.password.value;

        const res = await fetch('/login', {
            method: "POST",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify(loginInfo)
        });
        if (res.status === 200) {
            console.log(await res.json());
            window.location = '/main.html'
        }
        else{
            
            const result = await res.json();
            console.log(result)
            // document.querySelector(".show-message").innerText = `Wrong Username/Password!`
            alert(`Wrong Username/Password!`)
        }
        form.reset();
    });

document.querySelector('.fa-linkedin-in')
    .addEventListener('click', async () =>{
        const linkedinClientID = '86g6ek88x6udmu';
        const scope = 'r_liteprofile%20r_emailaddress'
        const state = '123456' // a value specific to this application, self-assignable
        const redirectURL = 'http%3A%2F%2Fwww.ynam.me%2Flogin%2Flinkedin' // redirect to localhost:8080. To be updated when deploy to server.
        window.location = `https://www.linkedin.com/oauth/v2/authorization?response_type=code&client_id=${linkedinClientID}&scope=${scope}&state=${state}&redirect_uri=${redirectURL}`
    })