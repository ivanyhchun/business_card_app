const qrcode = window.qrcode;

const video = document.createElement("video");
const canvasElement = document.getElementById("qr-canvas");
const canvas = canvasElement.getContext("2d");

const qrResult = document.getElementById("qr-result");
const outputData = document.getElementById("outputData");

let scanning = false;

qrcode.callback = res => {
  if (res) {   
    // outputData.innerText = res;
    video.srcObject.getTracks().forEach(track => {
      track.stop();
    });
    scanning = false;

    console.log(res)
    let name = res.split('\n')[4].split(":").pop();
    let org = res.split('\n')[5].split(":").pop();
    let title = res.split('\n')[6].split(":").pop();
    let email = res.split('\n')[7].split(":").pop();
    let phone = res.split('\n')[8].split(":").pop();
    let address = res.split('\n')[9].split(":").pop().replace(";;;;;;","")
    let query = encodeURI(`?name=${name}&org=${org}&title=${title}&email=${email}&phone=${phone}&address=${address}`)
    window.location = `/edit-scan-card.html${query}`


    // qrResult.hidden = false;
    // canvasElement.hidden = true;
    // window.location = './main.html'
  }
};

navigator.mediaDevices.getUserMedia({ video: { facingMode: "environment" } })
  .then((stream) => {
    scanning = true;
    qrResult.hidden = true;
    canvasElement.hidden = false;

    video.setAttribute("playsinline", true); // required to tell iOS safari we don't want fullscreen
    video.srcObject = stream;
    video.play();
    tick();
    scan();

  })

function tick() {
  canvasElement.height = video.videoHeight;
  canvasElement.width = video.videoWidth;
  let scanImg = canvas.drawImage(video, 0, 0, canvasElement.width, canvasElement.height);

  scanning && requestAnimationFrame(tick);
}

function scan() {
  try {
    qrcode.decode();
  } catch (e) {
    setTimeout(scan, 300);
  }
}



