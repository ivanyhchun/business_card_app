import Knex from 'knex';
const knexFile = require('../knexfile');
const knex = Knex(knexFile['test']);
import {UserService} from './UserServices'
import {User} from './models';

describe('UserService',()=>{

    let users:User[];
    let userService:UserService;

    beforeEach(async ()=>{
        users = [
            {
                email:"user1@gmail.com",
                display_name:'User 1',
                password: '12345678'
            },
        ]
        await knex.insert(users).into('users');
        userService = new UserService(knex);
    });


    it("should search member by username",async ()=>{
        const userReturned = await userService.login("user1@gmail.com");
        expect(userReturned).toMatchObject(users[0]);
    })

    it("should return new user",async ()=>{
        const newUserID = await userService.signUp('user3','user3@user3.com','user3password');
        const userReturned = await knex.select('*').from('users')
            .where('username','user3@user3.com');
        expect(userReturned[0].display_name).toMatch("user3");
        expect(userReturned[0].password).toMatch("user3password");
        console.log(newUserID)
    })

    it("should return new Linkedin User",async ()=>{
        const newUserID = await userService.linkedin('user4','user4@linkedin.com','user4password',"user4LinkedinID");
        const userReturned = await knex.select('*').from('users')
            .where('username','user4@linkedin.com');
        expect(userReturned[0].display_name).toEqual('user4')
        expect(userReturned[0].password).toEqual('user4password')
        expect(userReturned[0].linkedin_ac).toEqual('user4LinkedinID')
        console.log(newUserID)

    })

    afterEach(async ()=>{
        
        await knex('users').del(); 
    });

    afterAll(async ()=>{
        await knex.destroy();
    })
})