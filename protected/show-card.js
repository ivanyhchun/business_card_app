import * as a from './modules/qrcode.js'
import * as b from './modules/vcard-creator.js'

let VCard = window.vcardcreator.default
let myVCard = new VCard()

let profileId = window.location.search.replace("?id=","")
let canvas = document.getElementById('qrcode')
const res = await fetch (`/edit-my-card/?id=${profileId}`)
const result = await res.json() 

async function showCardImg(){
    console.log(result.namecard_img)
    document.querySelector('.card-img-container')
        .innerHTML += `
        <img src="./profile_namecard/${result.namecard_img}" alt="sample image" class="name-card-img">
        `
}

document.querySelector('.edit-btn')
        .addEventListener('click', async () => {
            window.location = `/my-own-cards?id=${profileId}`; 
})


showCardImg()

document.querySelector('.qr-btn').addEventListener('click',async ()=>{
    myVCard
        .addName(result.name)
        .addCompany(result.org)
        .addJobtitle(result.title)
        .addEmail(result.email)
        .addPhoneNumber(result.phone1,'WORK')
        .addAddress(result.address)
        .addURL(result.website)
        
    let VCardString = myVCard.toString()
    QRCode.toDataURL(VCardString, function (err, url) {
        console.log(url)
        })
    QRCode.toCanvas(canvas,VCardString,function(error){
        if (error) console.error(error)
        console.log('success!');
    })

})