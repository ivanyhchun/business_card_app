import * as a from './modules/interact.js'

// essential function for interact.js to be worked
function dragMoveListener(event) {
    var target = event.target
    var x = (parseFloat(target.getAttribute('data-x')) || 0) + event.dx
    var y = (parseFloat(target.getAttribute('data-y')) || 0) + event.dy

    target.style.transform = 'translate(' + x + 'px, ' + y + 'px)'

    target.setAttribute('data-x', x)
    target.setAttribute('data-y', y)
}
// this function is used later in the resizing and gesture demos
window.dragMoveListener = dragMoveListener

// Namecard Dropzone Definition


interact('.namecard-zone')
    .dropzone({
    accept: '.droppable',
    overlap: 0.75,

    ondropactivate: function (event) {
        event.target.classList.add('drop-active')
    },
    ondragenter: function (event) {
        var draggableElement = event.relatedTarget
        var dropzoneElement = event.target

        dropzoneElement.classList.add('drop-target')
        draggableElement.classList.add('can-drop')
        // draggableElement.textContent = 'Dragged in'
    },
    ondragleave: function (event) {
        event.target.classList.remove('drop-target')
        event.relatedTarget.classList.remove('can-drop')
        // event.relatedTarget.textContent = 'Dragged out'
    },
    ondrop: function (event) {
        event.relatedTarget.classList.add('dropped')
        // event.relatedTarget.textContent = 'Dropped'
    },
    ondropdeactivate: function (event) {
        event.target.classList.remove('drop-active')
        event.target.classList.remove('drop-target')
    }
})
// elements with drag-and-drop, resizable and content editable attributes
interact('.droppable-text')
    .draggable({
    listeners: { move: window.dragMoveListener },
    inertia: true,
    modifiers: [
        interact.modifiers.restrictRect({
        endOnly: true
        })
    ]
    })
    .on('doubletap', function (event) {
        event.currentTarget.setAttribute("contenteditable", "true");
    })


//  Trash bin Dropzone Definition
interact('.trash-zone')
    .dropzone({
    accept: '.droppable',
    // accept: '.droppable-image',
    overlap: 0.2,

    ondropactivate: function (event) {
        event.target.classList.add('drop-active')
    },
    ondragenter: function (event) {
        var draggableElement = event.relatedTarget
        var dropzoneElement = event.target

        dropzoneElement.classList.add('drop-target')
        draggableElement.classList.add('will-remove')
    },
    ondragleave: function (event) {
        event.target.classList.remove('drop-target')
        event.relatedTarget.classList.remove('will-remove')
    },
    ondrop: function (event) {
        event.relatedTarget.remove()
    },
    ondropdeactivate: function (event) {
        event.target.classList.remove('drop-active')
        event.target.classList.remove('drop-target')
    }
})
