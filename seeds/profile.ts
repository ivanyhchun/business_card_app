import { Knex } from "knex";

export async function seed(knex: Knex): Promise<void> {
    // Deletes ALL existing entries
    
    await knex("drawings").del();
    await knex("profile").del();
    await knex("namecards").del();
    await knex("category").del(); 
    await knex("my_category").del(); 
    await knex("users").del(); 
    
    
    
    const ids = await knex("users").insert([
        {
            display_name: "Test",
            email: "test@gmail.com",
            password: "$2a$10$BVjYlEm4/lg0WaIwAVHbteSDUfBr6k.tkW5zgCXEnSY1FmzpqUBFC"
        }
    ]).returning("id");

    const cat_ids = await knex("category").insert([
        {
            category_name: "Technology",
            users_id: ids[0]
        },
        {
            category_name: "Fashion",
            users_id: ids[0]
        },
        {
            category_name: "Social Media",
            users_id: ids[0]
        }
    ]).returning('id'); 

    const my_cat_ids = await knex("my_category").insert([
        {
            category_name: "Aviation",
            users_id: ids[0]
        },
        {
            category_name: "Advertising",
            users_id: ids[0]
        },
        {
            category_name: "Finance",
            users_id: ids[0]
        }
    ]).returning('id'); 

    // Inserts seed entries
    await knex("profile").insert([
        {  
            users_id: ids[0],
            name: "Tab Wong",
            org: "Cathay Pacific",
            address: "Cathay Pacific City, 8 Scenic Road, Hong Kong",
            phone1: "2747 3333",
            email: "tab_wong@cathaypacific.com",
            title: "Inflight Manager",
            profile_name: "CX",
            category_id: my_cat_ids[0]
        },
        { 
            users_id: ids[0],
            name: "Tab Wong",
            org: "ZenithOptimedia",
            address: "6/F, AIA Kowloon Tower, 100 How Ming Street, Kwun Tong",
            phone1: "2236 9000",
            email: "tab_wong@zenithoptimedia.com",
            title: "Media Manager",
            profile_name: "Media",
            category_id: my_cat_ids[1]
        },
        { 
            users_id: ids[0],
            name: "Tab Wong",
            org: "Convoy Financial Services Limited",
            address: "23/F, Hopewell Centre, 183 Queen's Road East, Wan Chai",
            phone1: "3601 5253",
            email: "tab.wong@convoy.com.hk",
            title: "Consultant",
            profile_name: "Liar",
            category_id: my_cat_ids[2]
        }
    ]);

    await knex("namecards").insert([
        {  
            users_id: ids[0],
            name: "Elon Musk",
            org: "SpaceX",
            address: "1 Rocket Road, Hawthorne, California 90250",
            phone1: "+1 310-363-6000",
            email: "elonmusk@spacex.com",
            title: "CEO",
            category_id: cat_ids[0]
        },
        {
            users_id: ids[0],
            name: "Steve Jobs",
            org: "Apple Inc.",
            address: "One Apple Park Way, Cupertino, CA 95014",
            phone1: "(852) 800 908 988",
            email: "appleid@id.apple.com",
            title: "CEO",
            category_id: cat_ids[0]
        },
        {
            users_id: ids[0],
            name: "Jeff Bezos",
            org: "Amazon.com, Inc.",
            address: "440 Terry Avenue, North Seattle, WA 98109",
            phone1: "001 206-922-0880",
            email: "",
            title: "CEO",
            category_id: cat_ids[0]
        },
        {
            users_id: ids[0],
            name: "Mark Zuckerberg",
            org: "Facebook, Inc.",
            address: "Facebook Headquarters, 1 Hacker Way, Menlo Park, CA 94025",
            phone1: "(650) 543-4800",
            email: "",
            title: "CEO",
            category_id: cat_ids[2]
        },
        {
            users_id: ids[0],
            name: "Andy Jassy",
            org: "Amazon Web Services, Inc.",
            address: "440 Terry Avenue, North Seattle, WA 98109",
            phone1: "001 206-922-0880",
            email: "",
            title: "CEO",
            category_id: cat_ids[0]
        },
        {
            users_id: ids[0],
            name: "Bernard Arnault",
            org: "LVMH",
            address: "24/F, 18 Oxford House, Westlands Road, Quarry Bay",
            phone1: "(852) 2881 1631",
            email: "bernard.arnault@lvmh.com",
            title: "CEO",
            category_id: cat_ids[1]
        },
        {
            users_id: ids[0],
            name: "Sundar Pichai",
            org: "Google LLC",
            address: "1600 Amphitheatre Parkway Mountain View, CA 94043",
            phone1: "(650) 253-0000",
            email: "press@google.com",
            title: "CEO",
            category_id: cat_ids[0]
        },
        {
            users_id: ids[0],
            name: "Jack Dorsey",
            org: "Twitter, Inc.",
            address: "1355 Market Street, San Francisco, California",
            phone1: "",
            email: "",
            title: "CEO",
            category_id: cat_ids[2]
        }
    ]);
};
