// install 'dotenv' and '@azure/ai-form-recognizer@3.1.0-beta.3' package to run this code
const { FormRecognizerClient, AzureKeyCredential } = require("@azure/ai-form-recognizer");

const fs = require("fs");
const dotenv = require("dotenv");
dotenv.config();

export async function recognizeCards(input:string) {
  // store the azure Key and Endpoint in a .env file and place at the same directory of this .js file
  const endpoint = "https://tecky-cohort14-namecard.cognitiveservices.azure.com/";
  const apiKey = "557b764712174645ac09246c061e80ba";

  const fileName = input;

  if (!fs.existsSync(fileName)) {
    throw new Error(`Expected file "${fileName}" to exist.`);
  }

  const readStream = fs.createReadStream(fileName);

  const client = new FormRecognizerClient(endpoint, new AzureKeyCredential(apiKey));
  const poller = await client.beginRecognizeBusinessCards(readStream, {
    contentType: "image/jpeg",
    onProgress: (state:{status:string}) => {
      console.log(`status: ${state.status}`);
    }
  });

  const [businessCard] = await poller.pollUntilDone();

  if (businessCard === undefined) {
    throw new Error("Failed to extract data from at least one business card.");
  }
  return businessCard
  
  // console.log(businessCard.fields[Departments]?.value)
  // console.log(businessCard.fields[MobilePhones]?.value)
//   console.log("Business Card Fields:");

//   const contactNames = businessCard.fields["ContactNames"].value;
//   if (Array.isArray(contactNames)) {
//     console.log("- Contact Names:");
//     for (const contactName of contactNames) {
//       if (contactName.valueType === "object") {
//         const firstName = contactName.value?.["FirstName"].value ?? "<no first name>";
//         const lastName = contactName.value?.["LastName"].value ?? "<no last name>";
//         console.log(`  - ${firstName} ${lastName} (${contactName.confidence} confidence)`);
//       }
//     }
//   }

//   printSimpleArrayField(businessCard, "CompanyNames");
//   printSimpleArrayField(businessCard, "Departments");
//   printSimpleArrayField(businessCard, "JobTitles");
//   printSimpleArrayField(businessCard, "Emails");
//   printSimpleArrayField(businessCard, "Websites");
//   printSimpleArrayField(businessCard, "Addresses");
//   printSimpleArrayField(businessCard, "MobilePhones");
//   printSimpleArrayField(businessCard, "Faxes");
//   printSimpleArrayField(businessCard, "WorkPhones");
//   printSimpleArrayField(businessCard, "OtherPhones");
// }

// function printSimpleArrayField(businessCard, fieldName) {
//   const fieldValues = businessCard.fields[fieldName]?.value;
//   if (Array.isArray(fieldValues)) {
//     console.log(`- ${fieldName}:`);
//     for (const item of fieldValues) {
//       console.log(`  - ${item.value ?? "<no value>"} (${item.confidence} confidence)`);
//     }
//   } else if (fieldValues === undefined) {
//     console.log(`No ${fieldName} were found in the document.`);
//   } else {
//     console.error(
//       `Error: expected field "${fieldName}" to be an Array, but it was a(n) ${businessCard.fields[fieldName].valueType}`
//     );
//   }
}

// configure the filepath for the namecard jpg file
// namecardPath = '../protected/nameCards/image-1620375512644.jpeg'

// recognizeCards(namecardPath).catch((err) => {
//   console.error("The sample encountered an error:", err);
// });