
function dragMoveListener(event) {
    var target = event.target
    var x = (parseFloat(target.getAttribute('data-x')) || 0) + event.dx
    var y = (parseFloat(target.getAttribute('data-y')) || 0) + event.dy

    target.style.transform = 'translate(' + x + 'px, ' + y + 'px)'

    target.setAttribute('data-x', x)
    target.setAttribute('data-y', y)
}

export function gesturableListener(){
    let angleScaleList = []
    let gestureAreaList = document.querySelectorAll('.droppable-image')
    let scaleElementList = document.querySelectorAll('.scale-element')

    for (let i = 0; i <gestureAreaList.length; i++) {
        angleScaleList.push({
            angle: 0,
            scale: 1
        })
        interact(gestureAreaList[i])
            .gesturable({
                listeners: {
                    start (event) {
                        angleScaleList[i].angle -= event.angle
                    },
                    move (event) {
                        var currentAngle = event.angle + angleScaleList[i].angle
                        var currentScale = event.scale * angleScaleList[i].scale
                        scaleElementList[i].style.transform =
                    'rotate(' + currentAngle + 'deg)' + 'scale(' + currentScale + ')'
                    dragMoveListener(event)
                    },
                    end (event) {
                        angleScaleList[i].angle = angleScaleList[i].angle + event.angle
                        angleScaleList[i].scale = angleScaleList[i].scale * event.scale
                        }
                    }
                })
            .draggable({
                listeners: { move: dragMoveListener }
            })
            .resizable({
                // resize from all edges and corners
                edges: { left: false, right: true, bottom: true, top: false },
            
                listeners: {
                    move(event) {
                        var target = event.target
                        var x = (parseFloat(target.getAttribute('data-x')) || 0)
                        var y = (parseFloat(target.getAttribute('data-y')) || 0)
            
                        // update the element's style
                        target.style.width = event.rect.width  + 'px'
                        target.style.height = event.rect.height + 'px'
            
                        // translate when resizing from top or left edges
                        x += event.deltaRect.left
                        y += event.deltaRect.top
            
                        target.style.transform = 'translate(' + x + 'px,' + y + 'px)'
            
                        target.setAttribute('data-x', x)
                        target.setAttribute('data-y', y)
                        // target.textContent = Math.round(event.rect.width) + '\u00D7' + Math.round(event.rect.height)
                    }
                },
                modifiers: [
                    // minimum size
                    interact.modifiers.restrictSize({
                        min: { width: 50, height: 20 }
                    })
                ],
                inertia: true
                })
    }
}