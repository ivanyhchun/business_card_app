async function changePassword(){
    document.querySelector('.form-control-group')
        .addEventListener('submit', async (event)=>{
            event.preventDefault();
            const form = event.target;
            let passwordInfo = {};
            passwordInfo.oldPassword = form.old_password.value
            passwordInfo.newPassword = form.new_password.value
            passwordInfo.confirmPassword = form.confirm_password.value
            
            if(passwordInfo.newPassword == passwordInfo.confirmPassword){
                const res = await fetch('/changePassword', {
                    method: "PUT",
                    headers: {
                        "Content-Type": "application/json"
                    },
                    body: JSON.stringify(passwordInfo)
                });
                
                await res.json();
                if (res.status === 201) {
                window.location = './profile.html'
                }
                else {
                    document.querySelector('.prompt-message').innerText = 'Old Passwords do not match'; 
                    form.reset()
                }
            }else{
                document.querySelector('.prompt-message').innerText = 'Passwords do not match';
            }
        })

}
changePassword()