async function loadProfile() {
    const res = await fetch('/own-cards');
    console.log(res);

    const profiles = await res.json();
    console.log(profiles);

    for (let profile of profiles) {
        document.querySelector('.card-container')
            .innerHTML += `
                <div class="card-group" data-index="${profile.id}">
                    <div class="card-field">
                        <div class="info-group">
                            <div class="card-name">${profile.profile_name}</div>
                            <div class="company-name">${profile.org}</div>
                            <div class="job-title">${profile.title}</div>
                        </div>
                        <div class="contact-group">
                            ${profile.phone1 ? `
                                <div class="flex-group phone-group">
                                    <i class="fas fa-phone-alt"></i>
                                    <div class="contact">${profile.phone1}</div>
                                </div>` : ""}
                            ${profile.email ? `
                                <div class="flex-group email-group">
                                    <i class="far fa-envelope"></i>
                                    <div class="email">${profile.email}</div>
                                </div>` : ""} 
                        </div>
                    </div>
                    <div class="delete-icon" data-index="${profile.id}">
                        <i class="fas fa-trash-alt"></i>
                    </div>
                </div>
            `
    }

    const deleteBtns = document.querySelectorAll('.delete-icon');

    for (let deleteBtn of deleteBtns) {
        deleteBtn.addEventListener('click', async (event) => {
            event.preventDefault();
            event.stopPropagation();

            const cfn = confirm('Confirm delete?');
            if (cfn) {
                const delBtnIdx = event.currentTarget.getAttribute('data-index');

                const res = await fetch(`/my-own-cards?id=${delBtnIdx}`, {
                    method: 'DELETE'
                });

                if (res.status === 200) {
                    window.location = '/own-cards.html';
                } else {
                    await res.json();
                }
            }
        }, true);
    }

    const ownCards = document.querySelectorAll('.card-group');

    for (let ownCard of ownCards) {
        ownCard.addEventListener('click', async (event) => {
            
            const cardId = event.currentTarget.getAttribute('data-index');

            console.log(cardId);

            window.location = `/show-card?id=${cardId}`; 
        })
    }
}

loadProfile();