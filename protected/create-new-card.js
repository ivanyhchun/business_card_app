async function createCard() {
    document.querySelector('.form-control-group')
        .addEventListener('submit', async (event) => {
            event.preventDefault(); 

            const form = event.target; 
            const formObject = {}; 

            formObject['profile_name'] = form.profile_name.value; 
            formObject['name'] = form.name.value; 
            formObject['org'] = form.org.value; 
            formObject['title'] = form.title.value; 
            formObject['address'] = form.address.value; 
            formObject['phone1'] = form.phone1.value; 
            formObject['email'] = form.email.value; 
            formObject['category'] = form.category.value; 
            console.log(formObject); 


            const res = await fetch('/own-new-card', {
                method: "POST",
                headers: {
                    "Content-Type": "application/json"
                },
                body: JSON.stringify(formObject)
            }); 
            if(res.status === 200) {
                const result = await res.json(); 
                const profileId = result.cardId.id;
                window.location = `./draw-card.html?id=${profileId}`;
            }

            form.reset(); 
        })
}

createCard(); 