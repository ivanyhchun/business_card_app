async function loadProfile(){
    let res = await fetch('/profileInfo')
    let info = await res.json();
    console.log(info)
    
    document.querySelector('.profile-container').innerHTML = `
        <div class="profile-img-container">
            <img src=${info.profile_pic? "/uploads/"+info.profile_pic : "/assets/profile1.png"} alt="" class="profile-img">
        </div>
        <div class="profile-info-container">
            <div class="display-name">
                ${info.display_name}
            </div>
            <div class="display-email">
                ${info.email}
            </div>
        </div>
    `
}

loadProfile()