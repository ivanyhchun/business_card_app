import { Knex } from 'knex';

export class ProfileService {

    constructor(private knex: Knex) {

    }

    async loadProfile(id: number) {
        return await this.knex.select('*').from('profile').where('users_id', id).orderBy('id');
    }

    async downloadDrawing(profileId: number) {
        return await this.knex.select('*').from('drawings').where('profile', profileId).first(); 
    }

    async uploadDrawing(profileId: number, content: string) {
        return await this.knex('drawings').insert({
            'profile': profileId,
            'html': content,
            'created_at': new Date(),
            'updated_at': new Date()
        }).onConflict('profile').merge(['html', 'updated_at']).returning('id'); 
    }

    async loadOwnCard(id: number) {
        return await this.knex.select('profile.*', 'my_category.category_name').from('profile')
            .innerJoin('my_category', 'category_id', 'my_category.id').where('profile.id', id).first();
    }

    async drawingToPNG(id: number, filepath: string) {
        return await this.knex('profile').insert({
            'id': id,
            'namecard_img': filepath,
            'created_at': new Date(),
            'updated_at': new Date()
        })
        .onConflict('id').merge(['namecard_img', 'updated_at']).returning('id'); 
    }

    async updateOwnCard(id: number, profile_name: string, name: string, org: string, title: string, address: string, phone1: string, email: string, category: string) {
        const trx = await this.knex.transaction();

        try {
            const checkId = await trx.select('id').from('my_category').where('category_name', category).first();

            let catId = 0;

            if (!checkId) {
                const catIdArr = await trx('my_category').insert({
                    'category_name': category,
                    'users_id': id
                }).returning('id');
                catId = catIdArr[0];
            } else {
                catId = checkId.id;
            }

            await trx('profile').where('id', id).update({
                'profile_name': profile_name,
                'name': name,
                'org': org,
                'title': title,
                'address': address,
                'phone1': phone1,
                'email': email,
                'category_id': catId
            })

            const result = await trx.select('*').from('profile').where('id', id).first();
            trx.commit();
            return result;
        } catch (error) {
            trx.rollback();
            throw error;
        }
    }

    async createCard(userId: number, profile_name: string, name: string, org: string, title: string, address: string, phone1: string, email: string, category: string) {
        const trx = await this.knex.transaction();
        try {

            const checkId = await trx.select('id').from('my_category').where('category_name', category).first();
            console.log(checkId);
            let catId = 0;
            if (!checkId) {
                const catIdArr = await trx('my_category').insert({
                    'category_name': category,
                    'users_id': userId
                }).returning('id');
                catId = catIdArr[0];
            } else {
                catId = checkId.id;
            }

            const cardId: number[] = await trx('profile').insert({
                'users_id': userId,
                'profile_name': profile_name,
                'name': name,
                'org': org,
                'title': title,
                'address': address,
                'phone1': phone1,
                'email': email,
                'category_id': catId
            }).returning('id');

            const result = await trx.select('*').from('profile').where('id', cardId[0]).first();
            await trx.commit();
            return result;

        } catch (error) {
            await trx.rollback();
            throw error;
        }
    }

    async deleteCard(cardId: number) {
        const trx = await this.knex.transaction();
        try {
            await trx('drawings').where('profile', cardId).del();
            await trx('profile').where('id', cardId).del();
            const result = await trx.select('*').from('profile');
            await trx.commit();
            return result;

        } catch (error) {
            await trx.rollback();
            throw error;
        }
    }

    async uploadCardBg(cardBgImg: string, profileId: number) {
        return await this.knex('drawings').update('bg_img', cardBgImg).where('profile', profileId); 
    }

}
