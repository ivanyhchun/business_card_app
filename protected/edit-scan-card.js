let x = "?name=Firstname%20Lastname%0D&org=Company%20Name%0D&title=Job%20TItle%0D&email=wbc@safsdfsa.com%0D&phone=12345676%0D&address=Comp%20Add%0D"

const query = location.search 

if (!query) {
    console.log("no query")
    updateFunctionByOCR()
} else {
    updateFunctionByQRCode()
}

async function updateFunctionByQRCode(){
    let queryDecoded = decodeURI(query).split("&")
    document.querySelector('.form-control-group')
        .innerHTML = `
        <div class="form-group">
            <div class="form-title">Name :</div>
            <input type="text" name="fullName" value="${queryDecoded[0].split("=").pop()}" class="form-control">
        </div> 
        <div class="form-group">
            <div class="form-title">Company Name :</div>
            <input type="text" name="company" value="${queryDecoded[1].split("=").pop()}" class="form-control">
        </div>   
        <div class="form-group">
        <div class="form-title">Title :</div>
        <input type="text" name="title" value="${queryDecoded[2].split("=").pop()}" class="form-control">
        </div>   
        <div class="form-group">
            <div class="form-title">Address :</div>
            <input type="text" name="addresses" value="${queryDecoded[5].split("=").pop()}" class="form-control">
        </div>   
        <div class="form-group">
            <div class="form-title">Mobile Number :</div>
            <input type="text" name="phone1" value="${queryDecoded[4].split("=").pop()}" class="form-control">
        </div> 
        <div class="form-group">
            <div class="form-title">Working Number :</div>
            <input type="text" name="phone2" value="" class="form-control">
        </div> 
        <div class="form-group">
            <div class="form-title">Email :</div>
            <input type="text" name="email" value="${queryDecoded[3].split("=").pop()}" class="form-control">
        </div>
        <div class="form-group">
            <div class="form-title">Website :</div>
            <input type="text" name="websites" value="" class="form-control">
        </div>           
        <div class="form-group">
            <div class="form-title">Linkedin :</div>
            <input type="text" name="linkedin" value="" class="form-control">
        </div>           
        <div class="form-group">
            <div class="form-title">Category :</div>
            <input type="text" name="category" value="" class="form-control">
        </div>           

        <div class="form-group">
            <input type="submit" value="Submit" class="submit-btn">
        </div>
    `

    let scanBtn = document.querySelector('.scanOrUpload');
    scanBtn.style.display = 'none';

    document.querySelector('.form-control-group')
    .addEventListener('submit', async (event) => {
        event.preventDefault();

        const form = event.target;
        const formObject = {};

        formObject['fullName'] = form.fullName.value;
        formObject['company'] = form.company.value;
        formObject['title'] = form.title.value;
        formObject['addresses'] = form.addresses.value;
        formObject['phone1'] = form.phone1.value;
        formObject['phone2'] = form.phone2.value;
        formObject['email'] = form.email.value;
        formObject['websites'] = form.websites.value;
        formObject['linkedin'] = form.linkedin.value;
        formObject['category'] = form.category.value;

        const res = await fetch('/create-scan-card', {
            method: "POST",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify(formObject)
        });
        if (res.status === 200) {
            const result = await res.json();
            window.location = './biz-cards.html';
        }
    })
}


async function updateFunctionByOCR() {
    document.querySelector('.scanOrUpload')
        .addEventListener('change', async (event) => {
            event.preventDefault();
            let formData = new FormData();
            formData.append('image', event.target.files[0]);
            const res = await fetch('/upload-namecard', {
                method: "POST",
                body: formData
            });

            if (res.status === 200) {
                const result = await res.json()

                console.log("123")
                console.log(result)
                document.querySelector('.form-control-group')
                    .innerHTML = `
            
                <div class="form-group">
                    <div class="form-title">Name :</div>
                    <input type="text" name="fullName" value="${result.name}" class="form-control">
                </div> 
                <div class="form-group">
                    <div class="form-title">Company Name :</div>
                    <input type="text" name="company" value="${result.company}" class="form-control">
                </div>   
                <div class="form-group">
                <div class="form-title">Title :</div>
                <input type="text" name="title" value="${result.title}" class="form-control">
                </div>   
                <div class="form-group">
                    <div class="form-title">Address :</div>
                    <input type="text" name="addresses" value="${result.addresses}" class="form-control">
                </div>   
                <div class="form-group">
                    <div class="form-title">Mobile Number :</div>
                    <input type="text" name="phone1" value="${result.phone1}" class="form-control">
                </div> 
                <div class="form-group">
                    <div class="form-title">Working Number :</div>
                    <input type="text" name="phone2" value="${result.phone2}" class="form-control">
                </div> 
                <div class="form-group">
                    <div class="form-title">Email :</div>
                    <input type="text" name="email" value="${result.email}" class="form-control">
                </div>
                <div class="form-group">
                    <div class="form-title">Website :</div>
                    <input type="text" name="websites" value="${result.websites}" class="form-control">
                </div>           
                <div class="form-group">
                    <div class="form-title">Linkedin :</div>
                    <input type="text" name="linkedin" value="" class="form-control">
                </div>           
                <div class="form-group">
                    <div class="form-title">Category :</div>
                    <input type="text" name="category" value="" class="form-control">
                </div>           

                <div class="form-group">
                    <input type="submit" value="Submit" class="submit-btn">
                </div>
                `

                let scanBtn = document.querySelector('.scanOrUpload');
                scanBtn.style.display = 'none';


                document.querySelector('.form-control-group')
                .addEventListener('submit', async (event) => {
                    event.preventDefault();
        
                    const form = event.target;
                    const formObject = {};
        
                    formObject['fullName'] = form.fullName.value;
                    formObject['company'] = form.company.value;
                    formObject['title'] = form.title.value;
                    formObject['addresses'] = form.addresses.value;
                    formObject['phone1'] = form.phone1.value;
                    formObject['phone2'] = form.phone2.value;
                    formObject['email'] = form.email.value;
                    formObject['websites'] = form.websites.value;
                    formObject['linkedin'] = form.linkedin.value;
                    formObject['category'] = form.category.value;
        
                    const res = await fetch('/create-scan-card', {
                        method: "POST",
                        headers: {
                            "Content-Type": "application/json"
                        },
                        body: JSON.stringify(formObject)
                    });
                    if (res.status === 200) {
                        const result = await res.json();
                        window.location = './biz-cards.html';
                    }
        
                })
                
            }
        })
}
