import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {
    if (await knex.schema.hasTable('drawings')){
        await knex.schema.alterTable('drawings', (table)=> {
                table.string('bg_img');
              })
    }
}


export async function down(knex: Knex): Promise<void> {
    if (await knex.schema.hasTable('drawings')){
        await knex.schema.alterTable('drawings', (table)=> {
            table.dropColumn('bg_img')
          })
    }
}

