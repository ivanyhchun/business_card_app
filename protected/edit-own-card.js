async function editOwnCard() {

    const id = window.location.search; 

    console.log(id); 

    const res = await fetch(`/edit-my-card/${id}`); 

    // console.log(res); 
    let ownCard = await res.json(); 
    console.log(ownCard); 
    document.querySelector('.form-control-group')
        .innerHTML = `
            
            <div class="form-group">
                <div class="form-title">Profile Name :</div>
                <input type="text" name="profile_name" value="${ownCard.profile_name}" class="form-control">
            </div> 
            <div class="form-group">
                <div class="form-title">Name :</div>
                <input type="text" name="name" value="${ownCard.name}" class="form-control">
            </div>  
            <div class="form-group">
                <div class="form-title">Company Name :</div>
                <input type="text" name="org" value="${ownCard.org}" class="form-control">
            </div>   
            <div class="form-group">
                <div class="form-title">Job Title :</div>
                <input type="text" name="title" value="${ownCard.title}"
                class="form-control">
            </div>
            <div class="form-group">
                <div class="form-title">Company Address :</div>
                <input type="text" name="address" value="${ownCard.address}" class="form-control">
            </div>          
            <div class="form-group">
                <div class="form-title">Contact Number :</div>
                <input type="text" name="phone1" value="${ownCard.phone1}"
                class="form-control">
            </div>         
            <div class="form-group">
                <div class="form-title">Email :</div>
                <input type="text" name="email" value="${ownCard.email}"
                class="form-control">
            </div>
            <div class="form-group">
                <div class="form-title">Category :</div>
                <input type="text" name="category" value="${ownCard.category_name}"
                class="form-control">
            </div>
            <div class="form-group">
                <input type="submit" value="Submit" class="submit-btn">
            </div>
        `

    document.querySelector('.form-control-group')
        .addEventListener('submit', async (event) => {
            event.preventDefault(); 

            const form = event.target; 
            const formObject = {}; 

            formObject['id'] = ownCard.id; 
            formObject['profile_name'] = form.profile_name.value; 
            formObject['name'] = form.name.value; 
            formObject['org'] = form.org.value; 
            formObject['title'] = form.title.value; 
            formObject['address'] = form.address.value; 
            formObject['phone1'] = form.phone1.value; 
            formObject['email'] = form.email.value; 
            formObject['category'] = form.category.value; 
            console.log(formObject); 

            const res = await fetch('/update-own-card', {
                method: "POST",
                headers: {
                    "Content-Type":"application/json"
                },
                body: JSON.stringify(formObject)
            }); 
            if(res.status === 200) {
                const result = await res.json(); 
                window.location = `./draw-card.html${id}`;
            }

            form.reset(); 

        })
    
}

editOwnCard(); 
