import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {
    if (await knex.schema.hasTable('namecards')){
        await knex.schema.alterTable('namecards', (table)=> {
                table.string('namecard_img');
                table.renameColumn('eng_name', 'name')
              })
    }
}


export async function down(knex: Knex): Promise<void> {
    if (await knex.schema.hasTable('namecards')){
        await knex.schema.alterTable('namecards', (table)=> {
            table.dropColumn('namecard_img')
            table.renameColumn('name', 'eng_name')
          })
    }
}

