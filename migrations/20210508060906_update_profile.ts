import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {
    if (await knex.schema.hasTable('profile')){
        await knex.schema.alterTable('profile', (table)=> {
                table.renameColumn('eng_name', 'name');
                table.renameColumn('eng_org', 'org');
                table.renameColumn('eng_address', 'address');
                table.renameColumn('eng_title', 'title');
              })
    }
}


export async function down(knex: Knex): Promise<void> {
    if (await knex.schema.hasTable('profile')){
        await knex.schema.alterTable('profile', (table)=> {
                table.renameColumn('name', 'eng_name');
                table.renameColumn('org', 'eng_org');
                table.renameColumn('address', 'eng_address');
                table.renameColumn('title', 'eng_title');
              })
    }
}
