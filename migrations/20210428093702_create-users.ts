import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {
    const hasTable = await knex.schema.hasTable('users');
    if(!hasTable){
        await knex.schema.createTable('users',(table)=>{
            table.increments();
            table.string("email").unique().notNullable()
            table.string("password").notNullable();
            table.string("display_name").notNullable();
            table.string("google_ac");
            table.string("linkedin_ac"); 
            table.string("profile_pic"); 
            table.timestamps(false,true); 
        })
    }
}


export async function down(knex: Knex): Promise<void> {
    await knex.schema.dropTableIfExists('users')
}

