import express from 'express';
import { ProfileService } from '../services/ProfileServices';
import path from 'path';

const fs = require("fs")

export class ProfileController {

    constructor(private profileService: ProfileService) {

    }

    loadProfile = async (req: express.Request, res: express.Response) => {
        try {
            const id = req.session['user'].id;
            const result = await this.profileService.loadProfile(id);
            
            res.status(200).json(result);
        } catch (error) {
            res.status(401);
            console.log(error);
        }

    }


    uploadPicture = async (req: express.Request, res: express.Response) => {
        try {
            const imagepath = req.file.filename;
            
            res.status(200).json({ success: true, path: imagepath }); 

        } catch (error) {
            res.status(401).json({ success: false }); 
            console.log(error); 
        }
    }


    downloadDrawing = async (req: express.Request, res: express.Response) => {
        try {
            const profileId = parseInt(req.params.id); 
            const result = await this.profileService.downloadDrawing(profileId); 
            
            res.status(200).json(result); 

        } catch (error) {
            res.status(401).json({ success: false }); 
            console.log(error); 
        }
    }

    uploadDrawing = async (req: express.Request, res: express.Response) => {
        try {
            const drawingHTML = req.body.content;
            const profileId = parseInt(req.params.id); 
            const result = await this.profileService.uploadDrawing(profileId, drawingHTML); 
            res.status(200).json(result); 
        } catch (error) {
            res.status(401).json({ success: false }); 
            console.log(error); 
        }
    }

    drawingToPNG = async (req: express.Request, res: express.Response) => {
        try {
            const profileId = parseInt(req.params.id); 
            
            const base64URL = req.body.base64URL; 
            let base64Image = base64URL.split(';base64,').pop();
            let nameCardImg = Date.now() + '.png'; 
            await fs.writeFile(path.resolve('./protected/profile_namecard/' + nameCardImg), base64Image, { encoding: 'base64' }, function (err: Error) {
                console.log(path.resolve('./protected/profile_namecard/' + nameCardImg));
            });
            const result = await this.profileService.drawingToPNG(profileId, nameCardImg); 
            if (Array.isArray(result)) {
                res.status(200).json({ success: true }); 
            } else {
                res.status(503).json({ success: false }); 
                console.log('upload failed'); 
            }

        } catch (error) {
            res.status(401).json({ success: false }); 
            console.log(error); 
        }
    }


    loadCardInfo = async (req: express.Request, res: express.Response) => {
        res.sendFile(path.resolve('./protected/edit-own-card.html'));
    }

    showCardImg = async (req: express.Request, res: express.Response) => {
        res.sendFile(path.resolve('./protected/show-card.html'));
    }


    loadOwnCard = async (req: express.Request, res: express.Response) => {

        try {
            const id = parseInt(String(req.query.id));
            
            const result = await this.profileService.loadOwnCard(id);
            console.log(result);
            res.status(200).json(result);
        } catch (error) {
            console.log(error);
            res.status(401).json({ success: false });
        }
    }

    updateOwnCard = async (req: express.Request, res: express.Response) => {
        try {
            const id = req.body.id;
            const profile_name = req.body.profile_name;
            const name = req.body.name;
            const org = req.body.org;
            const title = req.body.title;
            const address = req.body.address;
            const phone1 = req.body.phone1;
            const email = req.body.email;
            const category = req.body.category;

            await this.profileService.updateOwnCard(id, profile_name, name, org, title, address, phone1, email, category);
            res.status(200).json({ success: true });
        } catch (error) {
            res.status(401).json({ success: false });
            console.log(error);
        }
    }

    createCard = async (req: express.Request, res: express.Response) => {
        try {

            const userId = req.session['user'].id;

            const profile_name = req.body.profile_name;
            const name = req.body.name;
            const org = req.body.org;
            const title = req.body.title;
            const address = req.body.address;
            const phone1 = req.body.phone1;
            const email = req.body.email;
            const category = req.body.category;
            

            const cardId = await this.profileService.createCard(userId, profile_name, name, org, title, address, phone1, email, category);
            res.status(200).json({ cardId: cardId });
        } catch (error) {
            res.status(401).json({ success: false });
            console.log(error);
        }
    }

    deleteCard = async (req: express.Request, res: express.Response) => {
        try {

            const cardId = parseInt(String(req.query.id));

            const result = await this.profileService.deleteCard(cardId);
            res.status(200).json(result);

        } catch (error) {
            res.status(401).json({ success: false });
            console.log(error);
        }
    }
    uploadCardBg = async (req: express.Request, res: express.Response) => {
        try {
            const profileId = parseInt(String(req.query.id)); 
            if (req.file) {
                const cardBgImg = req.file.filename;
                await this.profileService.uploadCardBg(cardBgImg, profileId);
                res.status(200).json(cardBgImg); 
            }
        } catch (e) {
            console.log(e);
            res.status(401).json({ success: false }); 
        }
    }
}