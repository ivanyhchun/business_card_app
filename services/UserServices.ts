import { Knex } from 'knex';
import { User } from './models';

export class UserService {

    constructor(private knex: Knex) {

    }

    async login(email: string): Promise<User> {
        return await this.knex.select('*').from('users').where('users.email', email).first();
    }

    async editProfileName(id: number, displayName: string) {

        await this.knex('users').where('id', id).update('display_name', displayName);
        return await this.knex.select('*').from('users').where('id', id).first();
    }

    async editProfileImg(id: number, profileImg: string) {
        await this.knex('users').where('id', id).update('profile_pic', profileImg);
        return await this.knex.select('*').from('users').where('id', id).first();

    }


    async signUp(displayName: string, email: string, hashedPassword: string): Promise<{ id: number }[]> {
        return await this.knex('users').insert({
            'display_name': displayName,
            'email': email,
            'password': hashedPassword,
            'created_at': new Date(),
            'updated_at': new Date()
        }).returning('id'); 
        
    }

    
    async linkedin(displayName: string | undefined, email: string, hashedPassword: string, linkedinAC: string | undefined): Promise<{ id: number }[]> {
        return await this.knex('users').insert({
            'display_name': displayName,
            'email': email,
            'password': hashedPassword,
            'linkedin_ac': linkedinAC,
            'created_at': new Date(),
            'updated_at': new Date(),
        }).returning('id'); 
        
    }

    async updateLinkedin(email: string, linkedinAC: string) {
        await this.knex('users').where('email', email).update('linkedin_ac', linkedinAC);
    }
    async getInfo(userId: number) {
        return await this.knex.select('id', 'email', 'display_name', 'profile_pic').from('users').where('id', userId).first();
    }

    async changePassword(userId: number, userPassword: string) {
        return await this.knex('users').update('password', userPassword).where('id', userId).returning('id'); 
    }
}