import express from "express";
import { userController, profileController, namecardsController, upload, uploadCards } from "./main";



export const userRoutes = express.Router();

export function initRoute() {

    userRoutes.post('/signup', userController.signup);
    userRoutes.post("/login", userController.login);
    userRoutes.get('/login/linkedin', userController.linkedin);

    userRoutes.get("/profileInfo", userController.info);
    userRoutes.post('/edit-content', upload.single('image'), userController.editProfile);
    userRoutes.put('/changePassword', userController.chgPassword);

    userRoutes.get('/own-cards', profileController.loadProfile);
    userRoutes.get('/show-card', profileController.showCardImg);
    userRoutes.get('/my-own-cards', profileController.loadCardInfo);
    userRoutes.get('/edit-my-card', profileController.loadOwnCard);
    userRoutes.get('/draw-card/:id', profileController.downloadDrawing);
    userRoutes.post('/DwgToPNG/:id', profileController.drawingToPNG);
    userRoutes.post('/draw-card/:id', profileController.uploadDrawing);
    userRoutes.post('/update-own-card', profileController.updateOwnCard);
    userRoutes.post('/own-new-card', profileController.createCard);
    userRoutes.post('/upload-pic', upload.single('image'), profileController.uploadPicture);
    userRoutes.delete('/my-own-cards', profileController.deleteCard);
    userRoutes.post('/upload-card-bg', uploadCards.single('image'), profileController.uploadCardBg);

    userRoutes.get('/biz-cards', namecardsController.loadBizCards);
    userRoutes.post('/create-scan-card', namecardsController.createScanCard);
    userRoutes.post('/upload-namecard', uploadCards.single('image'), namecardsController.uploadNamecards);
    
    userRoutes.delete('/biz-cards', namecardsController.deleteCard);

}