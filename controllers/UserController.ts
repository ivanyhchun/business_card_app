import express from 'express';
import { checkPassword, hashPassword } from '../hash';
import crypto from "crypto";
import fetch from 'node-fetch'
import { UserService } from "../services/UserServices"



export class UserController {

    constructor(private userService: UserService) {

    }

    login = async (req: express.Request, res: express.Response) => {
        const inputEmail = req.body.email; 
        const users = await this.userService.login(inputEmail);
        const userFound = users;
        if (!userFound) {
            res.status(401).json({ success: false, message: "Incorrect Username/Password" });
            return;
        }

        const match = await checkPassword(req.body.password, userFound.password);
        if (match) {
            req.session['user'] = users;
            res.status(200).json({ success: true }); 
        } else {
            res.status(401).json({ 'success': 'false' }); 
        }
    }

    editProfile = async (req: express.Request, res: express.Response) => {
        const id = req.session['user'].id;
        try {
            if (req.body.display_name) {
                const editedName = req.body.display_name;
                await this.userService.editProfileName(id, editedName);
            }
            if (req.file) {
                const profileImg = req.file.filename;
                await this.userService.editProfileImg(id, profileImg);
            }
            res.status(200).json({ success: true }); 
        } catch (error) {
            res.status(401);
            console.log(error); 
        }

    }

    signup = async (req: express.Request, res: express.Response) => {
        
        try {
            const inputDisplayName = req.body.display_name;
            const inputEmail = req.body.email;
            const inputPassword = req.body.password;
            const hashedPassword = await hashPassword(inputPassword);
            const newUserID = await this.userService.signUp(inputDisplayName, inputEmail, hashedPassword);
            if (newUserID) {
                req.session['user'] = {
                    id: newUserID[0],
                    email: inputEmail,
                    display_name: inputDisplayName,
                    password:hashedPassword
                }
                res.status(201).json({ success: true }); 
            }
        } catch (error) {
            if (error.constraint == 'users_email_unique') {
                console.log(error)
                res.status(406).json({ success: false, message: "failed to create user. Try with another email" }); 
            } else {
                res.status(501).json({ success: false, message: "signup error" }); 
            }
        }
    }

    //social login: Linkedin
    linkedin = async (req: express.Request, res: express.Response) => {
        // Step 1: saved the Authorization Code the client get from Linkedin on its browser
        const linkedinAuthCode = req.query.code
        const linkedinForwardURL = 'http%3A%2F%2Fwww.ynam.me%2Flogin%2Flinkedin' // URL forward to localhost:8080. To be changed when deploy to AWS server
        if (!req.query.code) {
            // redirect back to index page if no linkedin code
            res.redirect('/')
        } else {
            // Step 2. Get Access Token from Linkedin with the client's authorization code.
            const fetchAccessToken = await fetch(`https://www.linkedin.com/oauth/v2/accessToken?grant_type=authorization_code&client_id=${process.env.LINKEDIN_CLIENT_ID}&client_secret=${process.env.LINKEDIN_CLIENT_SECRET}&code=${linkedinAuthCode}&redirect_uri=${linkedinForwardURL}`, {
                method: "POST",
                headers: { "Content-Type": "application/x-www-form-urlencoded" },
            });
            if (fetchAccessToken.status != 200) {
                res.status(401).json({ success: false, message: "Linkedin Authorization Failed" })
            } else {
                // Step 3: fetch Linkedin API to obtain user's linkedin account information with the Access Token
                const fetchAccessTokenJson = await fetchAccessToken.json();
                const accessToken = fetchAccessTokenJson.access_token;
                // basic linkedin member info
                const fetchLinkedinInfo = await fetch(`https://api.linkedin.com/v2/me`, { headers: { "Authorization": "Bearer " + accessToken } })
                const fetchLinkedinInfoJson = await fetchLinkedinInfo.json()
                // linkedin member email
                const fetchLinkedinEmail = await fetch(`https://api.linkedin.com/v2/emailAddress?q=members&projection=(elements*(handle~))`, {
                    headers: { "Authorization": "Bearer " + accessToken }
                })
                const fetchLinkedinEmailJson = await fetchLinkedinEmail.json()
                let userLinkedinInfo = {
                    firstname: fetchLinkedinInfoJson.localizedFirstName,
                    lastname: fetchLinkedinInfoJson.localizedLastName,
                    linkedinId: fetchLinkedinInfoJson.id,
                    email: fetchLinkedinEmailJson.elements[0]["handle~"].emailAddress,
                }
                let user = await this.userService.login(userLinkedinInfo.email)
                console.log(user)
                if (user) {
                    await this.userService.updateLinkedin(user.email,userLinkedinInfo.linkedinId)
                    req.session["user"] = user;
                    res.status(200).redirect("/main.html");
                }
                else {
                    const randomPassword = await hashPassword(crypto.randomBytes(30).toString());
                    let newUser = {
                        email: userLinkedinInfo.email,
                        password: randomPassword,
                        display_name: userLinkedinInfo.firstname + " " + userLinkedinInfo.lastname,
                        linkedin_ac: userLinkedinInfo.linkedinId,
                    }
                    const newUserID = await this.userService.linkedin(newUser.display_name, newUser.email, newUser.password, newUser.linkedin_ac)
                    req.session["user"] = newUser;
                    req.session['user'].id = newUserID
                    res.status(200).redirect("/main.html");
                }
            }
        }
    }

    info = async (req: express.Request, res: express.Response) => {
        const userId = req.session['user'].id; 
        
        const profileInfo = await this.userService.getInfo(userId);

        res.json(profileInfo); 

    }

    chgPassword = async (req: express.Request, res: express.Response) => {
        const userPassword = req.session['user'].password; 
        const oldPassword = req.body.oldPassword; 
        const userId = req.session['user'].id; 
        const match = await checkPassword(oldPassword , userPassword); 
        if (match) {
            const newPassword = await hashPassword(req.body.newPassword); 
            await this.userService.changePassword(userId, newPassword);
            res.status(201).json({ success: true, message: "success" }); 
        }
        else {
            res.status(401).json({ success: false, message: "failed" }); 
        }
    }
}


