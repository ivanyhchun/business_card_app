import { UserController } from './controllers/UserController'
import { UserService } from './services/UserServices';
import express from 'express';
import expressSession from 'express-session';
import path from "path";
import multer from "multer";
import { Client } from 'pg';
import KnexFunction from 'knex';
import grant from "grant";
import { initRoute, userRoutes } from './routes';
import { ProfileService } from './services/ProfileServices';
import { ProfileController } from './controllers/ProfileController';
import { NamecardsService } from './services/NamecardsServices';
import { NamecardsController } from './controllers/NamecardsController'; 
const knexfile = require('./knexfile');
const configEnv = process.env.NODE_ENV || 'development';
const knex = KnexFunction(knexfile[configEnv]);
const siteURL = "http://www.ynam.me" // to be updated when deployed for production

const client = new Client({
    database: process.env.DB_NAME,
    user: process.env.DB_USERNAME,
    password: process.env.DB_PASSWORD
});

const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, path.resolve("./protected/uploads"));
    },
    filename: function (req, file, cb) {
        cb(
            null,
            `${file.fieldname}-${Date.now()}.${file.mimetype.split("/")[1]}`
        );
    },
});

const storageCard = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, path.resolve("./protected/nameCards"));
    },
    filename: function (req, file, cb) {
        cb(
            null,
            `${file.fieldname}-${Date.now()}.${file.mimetype.split("/")[1]}`
        );
    },
});

export const upload = multer({ storage });
export const uploadCards = multer({ storage:storageCard });

client.connect();

const app = express();

app.use(express.urlencoded({ extended: true, limit:"10mb" }))
app.use(express.json({limit:"10mb"}));


const sessionMiddleware = expressSession({
    secret: 'digital business cards',
    resave: true,
    saveUninitialized: true
});

app.use(sessionMiddleware);

const grantExpress = grant.express({
    defaults: {
        origin: siteURL,
        transport: "session",
        state: true,
    },
    // google: {
    //     key: process.env.GOOGLE_CLIENT_ID || "",
    //     secret: process.env.GOOGLE_CLIENT_SECRET || "",
    //     scope: [
    //         "profile",
    //         "email",
    //         "https://www.googleapis.com/auth/drive.scripts",
    //     ],
    //     callback: "/login/google",
    // },
    linkedin:{
        key:process.env.LINKEDIN_CLIENT_ID || "",
        secret:process.env.LINKEDIN_CLIENT_SECRET,
        scope:[
            "r_emailaddress",
            "r_liteprofile"
        ],
        callback: "login/linkedin"
    }
});

app.use(grantExpress as express.RequestHandler);

const isLoggedInAPI = (req: express.Request, res: express.Response, next: express.NextFunction) => {
    if (req.session['user']) {
        next();
    } else {
        res.status(401).json({ message: "Unauthorized:user" });
    }
}

// app.post('/edit-content', upload.single('image'), isLoggedInAPI, async function(req, res) {
//     console.log(req.body); 
// })

app.get('/current-user', isLoggedInAPI, async function (req, res) {
    try {
        const userFound = req.session['user'];
        if (userFound) {
            res.json({
                username: userFound.username
            });
        } else {
            res.status(401).json({ message: "Unauthorized" });
        }
    } catch (err) {
        console.log(err);
    }
});

export const userService = new UserService(knex);
export const userController = new UserController(userService);
export const profileService = new ProfileService(knex); 
export const profileController = new ProfileController(profileService); 
export const namecardsService = new NamecardsService(knex); 
export const namecardsController = new NamecardsController(namecardsService); 

initRoute();
app.use("/", userRoutes);





app.get('/logout', (req, res) => {
    delete req.session['user'];
    res.redirect('/') // 302
    // res.json({ success: true })
});



app.use(express.static("public"));
app.use(isLoggedInAPI,express.static('protected'));

app.listen(8080, () => {
    console.log("Listening at http://localhost:8080)")
});